# node-webrtc-data-channel

I'm reading socket.io docs

https://socket.io/docs/

https://socket.io/docs/server-installation/

https://github.com/websockets/ws#websocket-compression

https://socket.io/docs/namespaces/

https://socket.io/docs/rooms/

https://github.com/socketio/socket.io-redis

https://socket.io/docs/using-multiple-nodes/

https://socket.io/docs/client-installation/

https://socket.io/docs/client-initialization/

https://www.npmjs.com/package/socket.io-client

https://socket.io/docs/server-api/

https://socket.io/docs/client-api/

---

Wasted a lot of time on trying to see the rooms the socket is in. Hmm.
And now I see the API is not clearly mentioned in the docs...hmm..

https://socket.io/docs/migrating-from-2-x-to-3-0/#Socket-join-and-Socket-leave-are-now-synchronous

`socket.rooms` just doesn't work, and not sure if `socket.join` works as I'm not
able to test it out with `socket.rooms`. I could try some communication may be.

---

I'm trying to enable Type check based on Typescript, but in JavaScript files.

https://duckduckgo.com/?q=enable+types+in+vs+code+for+javascript&ia=web

https://code.visualstudio.com/Docs/languages/javascript#_type-checking

https://code.visualstudio.com/docs/nodejs/working-with-javascript#_type-checking-javascript

https://stackoverflow.com/questions/63516353/how-do-i-enable-javascript-type-checking-in-vue-files-on-visual-studio-code

Just addin `// @ts-check` at the top of the file worked!! :D :D Okay! Here
after I'll use that in case I need to do type checking while doing development
;) :D

---

I got an error for using import statements

```bash
$ node server.js
(node:21654) Warning: To load an ES module, set "type": "module" in the package.json or use the .mjs extension.
/Users/karuppiahn/oss/gitlab.com/snapping-shrimp/spikes/node-webrtc-data-channel/server.js:2
import { Server } from 'socket.io'
^^^^^^

SyntaxError: Cannot use import statement outside a module
    at wrapSafe (internal/modules/cjs/loader.js:931:16)
    at Module._compile (internal/modules/cjs/loader.js:979:27)
    at Object.Module._extensions..js (internal/modules/cjs/loader.js:1035:10)
    at Module.load (internal/modules/cjs/loader.js:879:32)
    at Function.Module._load (internal/modules/cjs/loader.js:724:14)
    at Function.executeUserEntryPoint [as runMain] (internal/modules/run_main.js:60:12)
    at internal/main/run_main_module.js:17:47
```

Not sure what the `type` is, but I'm going to add the `type` in `package.json`
and check it out

and that worked!

```bash
$ node server.js
(node:23930) ExperimentalWarning: The ESM module loader is experimental.
```

Now I get a weird error

```bash
$ node client.js
(node:26317) ExperimentalWarning: The ESM module loader is experimental.
file:///Users/karuppiahn/oss/gitlab.com/snapping-shrimp/spikes/node-webrtc-data-channel/client.js:2
import { io } from 'socket.io-client'
         ^^
SyntaxError: The requested module 'socket.io-client' does not provide an export named 'io'
    at ModuleJob._instantiate (internal/modules/esm/module_job.js:97:21)
    at async ModuleJob.run (internal/modules/esm/module_job.js:143:20)
    at async Loader.import (internal/modules/esm/loader.js:182:24)
    at async Object.loadESM (internal/process/esm_loader.js:84:5)
$ node client.js
(node:27366) ExperimentalWarning: The ESM module loader is experimental.
file:///Users/karuppiahn/oss/gitlab.com/snapping-shrimp/spikes/node-webrtc-data-channel/client.js:2
import { lookup } from 'socket.io-client'
         ^^^^^^
SyntaxError: The requested module 'socket.io-client' does not provide an export named 'lookup'
    at ModuleJob._instantiate (internal/modules/esm/module_job.js:97:21)
    at async ModuleJob.run (internal/modules/esm/module_job.js:143:20)
    at async Loader.import (internal/modules/esm/loader.js:182:24)
    at async Object.loadESM (internal/process/esm_loader.js:84:5)
```

Actually, `socket.io-client` does export `io` and uses `lookup` internally.
Both don't work. Ideally `io` should work. Hmm.

I gave up and tried to go to the other side. I chose TypeScript and started
checking it out.

```bash
$ npx tsc --out client.js client.ts
npx: installed 1 in 1.018s
client.ts(1,1): error TS1148: Cannot compile modules unless the '--module' flag is provided.
client.ts(1,20): error TS2307: Cannot find module 'socket.io-client'.
```

But I guess it might be too much work for a spike. Going to try to revert back
all changes in Server and Client related to import statments I think. I used
`import` as the TypeScript checking was erroring about it. Hmm. But I did find
out that `rooms` in `Socket` is actually a function. Weird I didn't know this.

But API says differently

https://socket.io/docs/v3/server-api/index.html#socket-rooms

Maybe what I saw in TypeScript definition was just a way to show `rooms` as a
getter function, but that it would work fine with just `socket.rooms` and does
not need `socket.rooms()`

```bash
$ node server.js
2aCHYWPLUt2rVYUfAAAB connected
On connected, room names:
got request to join room
/Users/karuppiahn/oss/gitlab.com/snapping-shrimp/spikes/node-webrtc-data-channel/server.js:13
        console.log(`${typeof socket.rooms} room names: ${Object.keys(socket.rooms())}`);
                                                                             ^

TypeError: socket.rooms is not a function
    at Socket.<anonymous> (/Users/karuppiahn/oss/gitlab.com/snapping-shrimp/spikes/node-webrtc-data-channel/server.js:13:78)
    at Socket.emit (events.js:314:20)
    at Socket.onevent (/Users/karuppiahn/oss/gitlab.com/snapping-shrimp/spikes/node-webrtc-data-channel/node_modules/socket.io/dist/socket.js:254:20)
    at Socket._onpacket (/Users/karuppiahn/oss/gitlab.com/snapping-shrimp/spikes/node-webrtc-data-channel/node_modules/socket.io/dist/socket.js:217:22)
    at /Users/karuppiahn/oss/gitlab.com/snapping-shrimp/spikes/node-webrtc-data-channel/node_modules/socket.io/dist/client.js:205:28
    at processTicksAndRejections (internal/process/task_queues.js:79:11)
```

Okay, clearly `socket.rooms()` is not a function. Hmm

Damn it me. `socket.rooms` just works fine. I was assuming it to be an object,
but it's some sort of special object. Damn. Hmm. I can't do `Object.keys` with
it. But console log of just `socket.rooms` works fine. It's a set.

```bash
$ node server.js
WEXO4jBeKG59SKezAAAB connected
On connected, room names:
got request to join room
object room names:
object After join room. Now :
Set { 'WEXO4jBeKG59SKezAAAB', 'my-room' }
```

`Set` is a type in Js

https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Set

---

Cool! I was able to do some message broadcasting now! ;)

Server:

```bash
$ node server.js
CtrU8uLhA8lyZTDcAAAB connected
On connected, room names: CtrU8uLhA8lyZTDcAAAB
got request to join room
Before joining room, Rooms: CtrU8uLhA8lyZTDcAAAB
After joining room. Rooms : CtrU8uLhA8lyZTDcAAAB,my-room
CtrU8uLhA8lyZTDcAAAB is sending message to room "my-room: hi all!!"
aFxcEnXF2WnQfsJgAAAD connected
On connected, room names: aFxcEnXF2WnQfsJgAAAD
got request to join room
Before joining room, Rooms: aFxcEnXF2WnQfsJgAAAD
After joining room. Rooms : aFxcEnXF2WnQfsJgAAAD,my-room
aFxcEnXF2WnQfsJgAAAD is sending message to room "my-room: hi all!!"
iqsdQH8g67-riXAYAAAF connected
On connected, room names: iqsdQH8g67-riXAYAAAF
got request to join room
Before joining room, Rooms: iqsdQH8g67-riXAYAAAF
After joining room. Rooms : iqsdQH8g67-riXAYAAAF,my-room
iqsdQH8g67-riXAYAAAF is sending message to room "my-room: hi all!!"
```

First Client:

```bash
$ node client.js
connected
joining room now
Message from room "my-room": hi all!!
Message from room "my-room": hi all!!
```

Second Client:

```bash
$ node client.js
connected
joining room now
Message from room "my-room": hi all!!
```

Third Client:

```bash
$ node client.js
connected
joining room now
```

---

Next I need to try and use webrtc in this nodejs environment and try to make the
clients communicate in a peer to peer fashion, and use the server for just
bootstrapping purposes

---

Idea:

- Client connects to socket server
- Client is asked if they want to join or create a room
- Client says create room
- A random room is created for the client, and the client is put in that room
  by the server. Server tells the client it's room name.
- Client can share the room name to another client, a friend.
- Friend connects to server
- Friend is asked if they want to join or create a room
- Friend says join room and provides room name
- After joining the room, the friend can see all the messages posted in the room
  and is also able to post messages
- Client also is able to post messages and also read messages
- All this messaging is done using webrtc. Only for the initial bootstrap the
  socket server is used. To confirm this, shutdown the server and see how the
  two clients communicate with each other through webrtc

---

https://www.npmjs.com/search?q=nodejs%20input

https://www.npmjs.com/package/enquirer

https://github.com/enquirer/enquirer#select-prompt

https://www.npmjs.com/search?q=uuid

https://www.npmjs.com/package/uuid

---

```bash
$ node client.js
connected to bootstrap server
✔ What do you want to do? · createANewRoom
Joined room ee0a3c23-ef8e-44e9-9365-8543a4f00ee5
/Users/karuppiahn/oss/gitlab.com/snapping-shrimp/spikes/node-webrtc-data-channel/node_modules/simple-peer/index.js:70
        throw errCode(new Error('No WebRTC support: Specify `opts.wrtc` option in this environment'), 'ERR_WEBRTC_SUPPORT')
        ^

Error: No WebRTC support: Specify `opts.wrtc` option in this environment
    at new Peer (/Users/karuppiahn/oss/gitlab.com/snapping-shrimp/spikes/node-webrtc-data-channel/node_modules/simple-peer/index.js:70:23)
    at startOffP2P (/Users/karuppiahn/oss/gitlab.com/snapping-shrimp/spikes/node-webrtc-data-channel/client.js:65:16)
    at Socket.<anonymous> (/Users/karuppiahn/oss/gitlab.com/snapping-shrimp/spikes/node-webrtc-data-channel/client.js:53:3)
    at Socket.Emitter.emit (/Users/karuppiahn/oss/gitlab.com/snapping-shrimp/spikes/node-webrtc-data-channel/node_modules/component-emitter/index.js:145:20)
    at Socket.emitEvent (/Users/karuppiahn/oss/gitlab.com/snapping-shrimp/spikes/node-webrtc-data-channel/node_modules/socket.io-client/build/socket.js:242:20)
    at Socket.onevent (/Users/karuppiahn/oss/gitlab.com/snapping-shrimp/spikes/node-webrtc-data-channel/node_modules/socket.io-client/build/socket.js:229:18)
    at Socket.onpacket (/Users/karuppiahn/oss/gitlab.com/snapping-shrimp/spikes/node-webrtc-data-channel/node_modules/socket.io-client/build/socket.js:193:22)
    at Manager.<anonymous> (/Users/karuppiahn/oss/gitlab.com/snapping-shrimp/spikes/node-webrtc-data-channel/node_modules/component-bind/index.js:21:15)
    at Manager.Emitter.emit (/Users/karuppiahn/oss/gitlab.com/snapping-shrimp/spikes/node-webrtc-data-channel/node_modules/component-emitter/index.js:145:20)
    at Manager.ondecoded (/Users/karuppiahn/oss/gitlab.com/snapping-shrimp/spikes/node-webrtc-data-channel/node_modules/socket.io-client/build/manager.js:209:15) {
  code: 'ERR_WEBRTC_SUPPORT'
}
```

I need to add another module called `wrtc`. Missed that 😅

Wow, okay, so I was able to run the two clients and see an output ! :D :D

```bash
# Server
$ node server.js
KDlmJw2aZIbHi3COAAAB connected
JUL7LeY6prrz8mb8AAAD connected
```

```bash
# Peer 1
$ node client.js
connected to bootstrap server
✔ What do you want to do? · createANewRoom
Joined room 787d1770-94c2-40de-b8fa-704354233713
got a message from peer: hey, this is the peer with isInitiator - true, how is it going?
```

```bash
# Peer 2
$ node client.js
connected to bootstrap server
✔ What do you want to do? · joinExistingRoom
✔ Type the room ID · 787d1770-94c2-40de-b8fa-704354233713
Joined room 787d1770-94c2-40de-b8fa-704354233713
got a message from peer: hey, this is the peer with isInitiator - false, how is it going?
```

The only thing is that, with the current design and implementation, a client can
also join a room with a random name that they type. For example:

```bash
# Peer 1
$ node client.js
connected to bootstrap server
✔ What do you want to do? · joinExistingRoom
✔ Type the room ID · ok
Joined room ok
Error occurred in the P2P connection. Code: ERR_SET_REMOTE_DESCRIPTION
[DOMException [InvalidStateError]: Failed to set remote offer sdp: Called in wrong state: kHaveLocalOffer] {
  code: 'ERR_SET_REMOTE_DESCRIPTION'
}
```

```bash
# Peer 2
$ node client.js
connected to bootstrap server
✔ What do you want to do? · joinExistingRoom
✔ Type the room ID · ok
Joined room ok
```

When Peer 2 joins, Peer 1 errors out. The current doesn't handle this situation.
Ideally the server should create the rooms and manage it I think. I gotta think
about that part. One pro to server creating it is, client will not have access
to create a room - if they do, they can name the room anything, and it could be
guessed and unnecessary people could try to barge in - even that should be
handled, because any room name is still a random set of characters even if it's
a UUID and security should not be based on obscurity. Anyways. Server could
create a good room name - a UUID code and server should also ensure that any
room the user joins, already exists - this is for the join room action where the
user gives input for the room name. For create room action, user cannot give
any input in the ideal situation. This is something at a design level. I need to
think more on this :) Hmm.
