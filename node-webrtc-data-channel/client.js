const io = require("socket.io-client");
const socket = io("http://localhost:3000");
const { Input, Select } = require('enquirer');
const Peer = require('simple-peer')
const wrtc = require('wrtc')

const createANewRoom = 'createANewRoom'
const joinExistingRoom = 'joinExistingRoom'

let connectedThroughWebRTC = false

socket.on("connect", function () {
  console.log("connected to bootstrap server");
  // console.log("joining room now");
  // 
  // socket.emit("send-message-to-room-mates", "my-room", "hi all!!");

  if (!connectedThroughWebRTC) {
    const prompt = new Select({
      name: 'action',
      message: 'What do you want to do?',
      choices: [createANewRoom, joinExistingRoom]
    });

    prompt.run()
      .then(answer => processAction(answer))
      .catch(processError);
  }

});

function processAction(action) {
  if (action !== createANewRoom && action !== joinExistingRoom) {
    console.error('unknown action has been chosen')
    process.exit(1)
  }

  if (action === createANewRoom) {
    createRoom();
    return
  }

  joinRoom();
}

function createRoom() {
  socket.emit('create-room')
}

function joinRoom() {
  const prompt = new Input({
    message: 'Type the room ID'
  });

  prompt.run()
    .then(roomName => socket.emit('join-room', roomName))
    .catch(processError);
}

socket.on('joined-room', (roomName) => {
  console.log(`Joined room ${roomName}`);
})

socket.on('start-p2p-connection', (roomName, isInitiator) => {
  startOffP2P(roomName, isInitiator, socket)
})

socket.on("disconnect", function () {
  console.log("disconnected from bootstrap server");
});

socket.on("on-message-from-room", function (room, message) {
  console.log(`Message from room "${room}": ${message}`);
});

function startOffP2P(roomName, isInitiator, socket) {
  const peer = new Peer({ initiator: isInitiator, wrtc: wrtc })

  socket.on('signal', data => {
    peer.signal(data)
  })

  peer.on('signal', data => {
    socket.emit('signal', roomName, data)
  })

  peer.on('connect', () => {
    connectedThroughWebRTC = true
    peer.send(`hey, this is the peer with isInitiator - ${isInitiator}, how is it going?`)
  })

  peer.on('data', data => {
    console.log('got a message from peer: ' + data)
  })

  peer.on('close', () => {
    connectedThroughWebRTC = false
  })

  peer.on('error', (err) => {
    console.log(`Error occurred in the P2P connection. Code: ${err.code}`);
    processError(err)
  })
}

function processError(err) {
  console.error(err)
  process.exit(1)
}
