const Server = require('socket.io')
const { v4: uuidv4 } = require('uuid');

const io = Server()

io.on('connect', socket => {
    console.log(`${socket.id} connected`);

    socket.on('create-room', () => {
        createRoom(socket)
    })

    socket.on('join-room', (roomName) => {
        joinRoom({ socket, roomName, isInitiator: true })
    })

    socket.on('send-message-to-room-mates', function (room, message) {
        console.log(`${socket.id} is sending message to room "${room}: ${message}"`);
        socket.to(room).emit('on-message-from-room', room, message)
    })

    socket.on('signal', (roomName, data) => {
        socket.to(roomName).emit('signal', data)
    })
});

function createRoom(socket) {
    const roomName = uuidv4()
    joinRoom({ socket, roomName, isInitiator: false })
}

function joinRoom({ socket, roomName, isInitiator }) {
    socket.join(roomName)
    socket.emit('joined-room', roomName)
    socket.emit('start-p2p-connection', roomName, isInitiator)
}



function getRoomNamesFromRoomsSet(socketRoomsSet) {
    return Array.from(socketRoomsSet)
}

io.listen(3000);
